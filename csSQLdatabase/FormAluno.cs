﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using System.Data.SQLite;

namespace csSQLdatabase
{
    /// <summary>
    /// Esta Form é chamada por:
    /// - qualquer das opções "inserir alunos", dos vários menus;
    /// - FormLista para alterar ou eliminar registos existentes.
    /// Tem um compartamento multiplo, atraves do construtor da Form e o metodo Load()
    /// - O construtor é alterado e recebe 3 parametros:
    /// - O metodo Load(), 1º dos metodos a ser executado após o construtor, prepara a Form;
    /// - O metodo click do botão ok, executa o SQL DML, de acordo com dmlSelect recebido
    /// </summary>
    public partial class FormAluno : Form
    {
        Form main;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert

        /*private SqlConnection sqlConn = null;
        private MySqlConnection mySqlConn = null;
        private SQLiteConnection sqlite = null;*/

        private SqlTransaction sqlTran = null;
        private MySqlTransaction mySqlTran = null;
        private SQLiteTransaction sqliteTran = null;
        /*
         * Construtor publico de 4 argumentos, 3 strings e 1 Form para a referencia
         * Origem:
         * -Menu vem um pedido dmlSelect = "Inserir" para um determinado sgbd para um codigo selecionado
         * -FormList vem um sgbd = "Update" ou "Delete" para um sgbd, para um codigo selecionado na DataGrid
         */
        public FormAluno(Form form)
        {
            InitializeComponent();
            main = form;
        }

        public FormAluno(Form form, String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            main = form;

            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;

            
        }

        //Load: 1º metododo a ser executado, depois do construtor
        private void FormAluno_Load(object sender, EventArgs e)
        {
            //Adiciona o nome da BD e do comnado sql a usar no botão OK, ao titulo da form
            this.Text = BDsgbd + " " + BDdml;

            //MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);

            //Alterar o nome do botão ok, de acordo com o dml
            btnOK.Text = BDdml;


            
            //se Insert, query à BD para obter o ultimo codigo pk

            //Se Update, usar o código passado no construtor para recolher os dados da BD

            //Se delete, usar o codigo passado no construtor para recolher os dados da BD
            //Definir os campos disable

            switch(BDdml)
            {
                case "Inserir":
                    textBoxNProc.Text = getLastTablePk();
                    textBoxNome.Focus();
                    break;

                case "Update":
                    getFieldsData();
                    textBoxNProc.Text = BDcodigo;
                    textBoxNome.Focus();              //Focus nesta caixa
                    break;

                case "Delete":
                    getFieldsData();
                    textBoxNumAluno.Enabled = false;
                    textBoxNome.Enabled = false;
                    textBoxNProc.Text = BDcodigo;
                    btnOK.Focus();
                    break;

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            main.Show();
        }

        private void FormAluno_Shown(object sender, EventArgs e)
        {
            MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }

        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
                //Recebe a UtilsSQl uma ligação ao sgbd
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();
            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                        
                        SqlCommand sqlCommand = new SqlCommand("Select MAX(nProc) as nProc from Aluno", sqlConn);   //Comando SQL DML
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if(sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while(sqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqlDataReader["nProc"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if(UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormAluno");
                                if( temp == "" )
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito" , "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT." , "TESTES; FormAluno");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo " , "INFO");
                            pkCode = 1;
                        }
                        break;
                    case "MySQL": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                        
                        MySqlCommand mySqlCommand = new MySqlCommand("Select MAX(nProc) as nProc from ALuno", mySqlConn);   //Comando SQL DML
                        mySqlCommand.CommandType = CommandType.Text;
                        
                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mySqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = mySqlDataReader["nProc"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if(UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormAluno");
                                if( temp == "" )
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito" , "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT." , "TESTES; FormAluno");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo " , "INFO");
                            pkCode = 1;
                        }
                        break;
                    case "SQL Lite": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                        
                        SQLiteCommand sqlLiteCommand = new SQLiteCommand("Select MAX(nProc) as nProc from Aluno", sqlite);   //Comando SQL DML
                        sqlLiteCommand.CommandType = CommandType.Text;
                        
                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqlLiteDataReader = sqlLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlLiteDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqlLiteDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqlLiteDataReader["nProc"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if(UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormAluno");
                                if( temp == "" )
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito" , "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT." , "TESTES; FormAluno");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo " , "INFO");
                            pkCode = 1;
                        }
                        break;
                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - getLastTablePK");
                        break;
                }
                
                
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormAluno");

            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;
                    case "MySQL": mySqlConn.Close();
                        break;
                    case "SQL Lite": sqlite.Close();
                        break;
                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - getLastTablePK");
                        break;
                }
            }
            return pkCode.ToString();
            
        }

        private void getFieldsData()
        {
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();
            if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server":

                        
                        SqlCommand sqlCommand = new SqlCommand("Select NAluno, nome from Aluno where nProc = @codigo", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                            //Enquanto houver rows
                            {
                                textBoxNome.Text = sqlDataReader["Nome"].ToString();
                                textBoxNumAluno.Text = sqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;
                    case "MySQL":
                        
                        MySqlCommand mysqlCommand = new MySqlCommand("Select NAluno, nome from ALuno where nProc = @codigo", mySqlConn);    //Comando SQL DML
                        mysqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        mysqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mysqlDataReader = mysqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mysqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (mysqlDataReader.Read())                            //Enquanto houver rows
                            {
                                textBoxNome.Text = mysqlDataReader["Nome"].ToString();
                                textBoxNumAluno.Text = mysqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;
                    case "SQL Lite":
                        
                        SQLiteCommand sqliteCommand = new SQLiteCommand("Select NAluno, nome from Aluno where nProc = @codigo", sqlite);    //Comando SQL DML
                        sqliteCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqliteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqliteDataReader = sqliteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqliteDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqliteDataReader.Read())                            //Enquanto houver rows
                            {
                                textBoxNome.Text = sqliteDataReader["Nome"].ToString();
                                textBoxNumAluno.Text = sqliteDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - getFieldsData");
                        break;
                }
                
            }
            catch(Exception e)
            {
                MessageBox.Show("Erro BD:\n" + e.Message, "FormAluno - getFieldsData()");
            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;
                    case "MySQL": mySqlConn.Close();
                        break;
                    case "SQL Lite": sqlite.Close();
                        break;
                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - getFieldsData");
                        break;
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            
            /*
             * SQL Transaction
             * Dá ordem ao SGBD para:
             * 1 Bloquear os objetos da base dados envolvidos
             * 2 Executa os comandos DML
             * 3 Liberta os objetos
             * 
             * Se erro num dos comandos DML
             * -> desfaz todos os outros (tenta)
             */
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();
            //validação de dados
            if (textBoxNProc.Text == "" || textBoxNumAluno.Text == "" || textBoxNome.Text == "")
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server":
                            
                            switch (BDdml)
                            {
                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                    
                                    SqlCommand sqlInsert = new SqlCommand("Insert into Aluno (nProc, NAluno, Nome) VALUES(@nProc, @NAluno, @Nome)", sqlConn);
                                    sqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    sqlInsert.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    sqlInsert.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                    sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                                    sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlUpdate = new SqlCommand("Update Aluno SET NAluno = @nAluno, Nome = @nome WHERE nProc = @nProc", sqlConn);
                                    sqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    sqlUpdate.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    sqlUpdate.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlDelete = new SqlCommand("Delete from Aluno where nProc = @nProc", sqlConn);
                                    sqlDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                    break;
                                 
                            }
                            break;
                        case "MySQL":
                            
                            switch (BDdml)
                            {
                                case "Inserir":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                    

                                    MySqlCommand MysqlInsert = new MySqlCommand("Insert into ALuno (nProc, NAluno, Nome) VALUES(@nProc, @NAluno, @Nome)", mySqlConn);
                                    MysqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    MysqlInsert.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    MysqlInsert.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    mySqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    mySqlTran = mySqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                    MysqlInsert.Transaction = mySqlTran;                    //Ligação dos comandos à transação
                                    MysqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    mySqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                    

                                    MySqlCommand mySqlUpdate = new MySqlCommand("Update ALuno SET NAluno = @nAluno, Nome = @nome WHERE nProc = @nProc", mySqlConn);
                                    mySqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    mySqlUpdate.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    mySqlUpdate.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlDelete = new MySqlCommand("Delete from ALuno where nProc = @nProc", mySqlConn);
                                    mySqlDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                    break;
                            }
                            break;
                        case "SQL Lite": 
                            switch (BDdml)
                            {
                                case "Inserir":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");


                                    SQLiteCommand sqliteInsert = new SQLiteCommand("Insert into Aluno (nProc, NAluno, Nome) VALUES(@nProc, @NAluno, @Nome)", sqlite);
                                    sqliteInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    sqliteInsert.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    sqliteInsert.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    sqlite.Open();                                     //abertura da ligaçã ao sgbd
                                    sqliteTran = sqlite.BeginTransaction();               //transação para controlo da operação SQL
                                    sqliteInsert.Transaction = sqliteTran;                    //Ligação dos comandos à transação
                                    sqliteInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqliteTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");


                                    SQLiteCommand sqliteUpdate = new SQLiteCommand("Update Aluno SET NAluno = @nAluno, Nome = @nome WHERE nProc = @nProc", sqlite);
                                    sqliteUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    sqliteUpdate.Parameters.AddWithValue("@NAluno", int.Parse(textBoxNumAluno.Text));
                                    sqliteUpdate.Parameters.AddWithValue("@Nome", textBoxNome.Text);

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqliteUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqliteDelete = new SQLiteCommand("Delete from Aluno where nProc = @nProc", sqlite);
                                    sqliteDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqliteDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                    break;
                            }
                            break;
                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - BtnOK");
                            break;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch(Exception exRollback)
                    {
                        MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                    }
                }
                finally
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server": sqlConn.Close();
                            break;

                        case "MySQL": 
                            mySqlConn.Close();
                            break;

                        case "SQL Lite":
                            sqlite.Close();
                            break;

                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - BtnOK - finaly");
                            break;
                    }
                    MessageBox.Show("Procedimento concuido", "INFO");
                    main.Show();
                    this.Close();
                }
                
            }
        }
        
    }
}
