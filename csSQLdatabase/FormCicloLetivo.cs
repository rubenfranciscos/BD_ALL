﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;

namespace csSQLdatabase
{
    public partial class FormCicloLetivo : Form
    {
        Form main;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert
        
        private SqlTransaction sqlTran = null;
        private MySqlTransaction mySqlTran = null;
        private SQLiteTransaction sqliteTran = null;
        /*
         * Construtor publico de 4 argumentos, 3 strings e 1 Form para a referencia
         * Origem:
         * -Menu vem um pedido dmlSelect = "Inserir" para um determinado sgbd para um codigo selecionado
         * -FormList vem um sgbd = "Update" ou "Delete" para um sgbd, para um codigo selecionado na DataGrid
         */
        public FormCicloLetivo(Form form)
        {
            InitializeComponent();
            main = form;
        }

        public FormCicloLetivo(Form form, String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            main = form;

            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;

            
        }

        private void FormCicloLetivo_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.CicloLetivo' table. You can move, or remove it, as needed.
            this.cicloLetivoTableAdapter.Fill(this.bDlocalDataSet.CicloLetivo);

            //Adiciona o nome da BD e do comnado sql a usar no botão OK, ao titulo da form
            this.Text = BDsgbd + " " + BDdml;

            //MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);

            //Alterar o nome do botão ok, de acordo com o dml
            btnOK.Text = BDdml;

            //se Insert, query à BD para obter o ultimo codigo pk

            //Se Update, usar o código passado no construtor para recolher os dados da BD

            //Se delete, usar o codigo passado no construtor para recolher os dados da BD
            //Definir os campos disable

            switch (BDdml)
            {
                case "Inserir":
                    nCicloTextBox.Text = getLastTablePk();
                    descrTextBox.Focus();
                    break;

                case "Update":
                    getFieldsData();
                    nCicloTextBox.Text = BDcodigo;
                    descrTextBox.Focus();              //Focus nesta caixa
                    break;

                case "Delete":
                    getFieldsData();
                    descrTextBox.Enabled = false;
                    nCicloTextBox.Text = BDcodigo;
                    btnOK.Focus();
                    break;

            }

        }

        private void cicloLetivoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cicloLetivoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            main.Show();
        }

        private void FormCicloLetivo_Shown(object sender, EventArgs e)
        {
            MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }


        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();     //Recebe a UtilsSQl uma ligação ao sgbd
            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        SqlCommand sqlCommand = new SqlCommand("Select MAX(nCiclo) as nCiclo from CicloLetivo", sqlConn);   //Comando SQL DML
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqlDataReader["nCiclo"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCicloLetivo");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormCicloLetivo");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        }

                        break;

                    case "MySQL":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        MySqlCommand mySqlCommand = new MySqlCommand("Select MAX(nCiclo) as nCiclo from CicloLetivo", mySqlConn);   //Comando SQL DML
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mySqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = mySqlDataReader["nCiclo"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCicloLetivo");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormCicloLetivo");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        }
                        break;

                    case "SQL Lite":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select MAX(nCiclo) as nCiclo from CicloLetivo", sqlite);   //Comando SQL DML
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqLiteDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqLiteDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqLiteDataReader["nCiclo"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCicloLetivo");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormCicloLetivo");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        }
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - getLastTblePK - finaly");
                        break;
                }
                

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormCicloLetivo");

            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - getLastTblePK - finaly");
                        break;
                }
            }
            return pkCode.ToString();

        }

        private void getFieldsData()
        {
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();

            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        SqlCommand sqlCommand = new SqlCommand("Select Descr from CicloLetivo where nCiclo = @codigo", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                            //Enquanto houver rows
                            {
                                descrTextBox.Text = sqlDataReader["Descr"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;

                    case "MySQL":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        MySqlCommand mySqlCommand = new MySqlCommand("Select Descr from CicloLetivo where nCiclo = @codigo", mySqlConn);    //Comando SQL DML
                        mySqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        mySqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mySqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                            //Enquanto houver rows
                            {
                                descrTextBox.Text = mySqlDataReader["Descr"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;

                    case "SQL Lite":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select Descr from CicloLetivo where nCiclo = @codigo", sqlite);    //Comando SQL DML
                        sqLiteCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqLiteDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqLiteDataReader.Read())                            //Enquanto houver rows
                            {
                                descrTextBox.Text = sqLiteDataReader["Descr"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - getFieldsData - finaly");
                        break;
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro BD:\n" + e.Message, "FormCicloLetivo - getFieldsData()");
            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - BtnOK - finaly");
                        break;
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();
            /*
             * SQL Transaction
             * Dá ordem ao SGBD para:
             * 1 Bloquear os objetos da base dados envolvidos
             * 2 Executa os comandos DML
             * 3 Liberta os objetos
             * 
             * Se erro num dos comandos DML
             * -> desfaz todos os outros (tenta)
             */
            SqlTransaction sqlTran = null;

            //validação de dados
            if (nCicloTextBox.Text == "" || descrTextBox.Text == "")
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server": 
                            switch (BDdml)
                            {

                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlInsert = new SqlCommand("Insert into CicloLetivo (nCiclo, Descr) VALUES(@nProc, @NAluno)", sqlConn);
                                    sqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    sqlInsert.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                    sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                                    sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlUpdate = new SqlCommand("Update CicloLetivo SET Descr = @nAluno WHERE nCiclo = @nProc", sqlConn);
                                    sqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    sqlUpdate.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlDelete = new SqlCommand("Delete from CicloLetivo where nCiclo = @nProc", sqlConn);
                                    sqlDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: CicloLetivo - Botão OK - switch default");
                                    break;

                            }
                            break;

                        case "MySQL":
                            switch (BDdml)
                            {

                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlInsert = new MySqlCommand("Insert into CicloLetivo (nCiclo, Descr) VALUES(@nProc, @NAluno)", mySqlConn);
                                    mySqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    mySqlInsert.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    mySqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    mySqlTran = mySqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                    mySqlInsert.Transaction = mySqlTran;                    //Ligação dos comandos à transação
                                    mySqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    mySqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlUpdate = new MySqlCommand("Update CicloLetivo SET Descr = @nAluno WHERE nCiclo = @nProc", mySqlConn);
                                    mySqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    mySqlUpdate.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlDelete = new MySqlCommand("Delete from CicloLetivo where nCiclo = @nProc", mySqlConn);
                                    mySqlDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: CicloLetivo - Botão OK - switch default");
                                    break;

                            }
                            break;

                        case "SQL Lite":
                            switch (BDdml)
                            {

                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqlLiteInsert = new SQLiteCommand("Insert into CicloLetivo (nCiclo, Descr) VALUES(@nProc, @NAluno)", sqlite);
                                    sqlLiteInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    sqlLiteInsert.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    sqlite.Open();                                     //abertura da ligaçã ao sgbd
                                    sqliteTran = sqlite.BeginTransaction();               //transação para controlo da operação SQL
                                    sqlLiteInsert.Transaction = sqliteTran;                    //Ligação dos comandos à transação
                                    sqlLiteInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqliteTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqLiteUpdate = new SQLiteCommand("Update CicloLetivo SET Descr = @nAluno WHERE nCiclo = @nProc", sqlite);
                                    sqLiteUpdate.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));
                                    sqLiteUpdate.Parameters.AddWithValue("@NAluno", descrTextBox.Text);

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqLiteUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqLiteDelete = new SQLiteCommand("Delete from CicloLetivo where nCiclo = @nProc", sqlite);
                                    sqLiteDelete.Parameters.AddWithValue("@nProc", int.Parse(BDcodigo));

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqLiteDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: CicloLetivo - Botão OK - switch default");
                                    break;

                            }
                            break;

                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - BtnOK - finaly");
                            break;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: CicloLetivo - Botão OK - switch()");
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: CicloLetivo - Botão OK - switch rollback");
                    }
                }
                finally
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server": sqlConn.Close();
                            break;

                        case "MySQL":
                            mySqlConn.Close();
                            break;

                        case "SQL Lite":
                            sqlite.Close();
                            break;

                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form CicloLetivo - BtnOK - finaly");
                            break;
                    }
                    MessageBox.Show("Procedimento concuido", "INFO");
                    main.Show();
                    this.Close();
                }

            }
        }
    }
}
