﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csSQLdatabase
{
    public partial class Jayce : Form
    {
        Form main;
        int x = 25, y = 25;
        int x2 = 60, y2 = 60;
        int salto = 16, salto2 = 6;
        Point mouse;

        public Jayce(Form fmain)
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            main = fmain;
        }

        private void Jayce_Click(object sender, EventArgs e)
        {
            this.Close();
            main.Show();
        }

        private void Jayce_Load(object sender, EventArgs e)
        {
            cicloDeVida.Enabled = true;
        }

        private void cicloDeVida_Tick(object sender, EventArgs e)
        {
            mouse = this.PointToClient(Cursor.Position);

            //catalin
            if(mouse.X > (x+25))
            {
                x += salto;
            }
            else
            {
                x -= salto;
            }

            if (mouse.Y > (y+25))
            {
                y += salto;
            }
            else
            {
                y -= salto;
            }


            //Ruben
            if(mouse.X > (x2+60))
            {
                x2 += salto2;
            }
            else
            {
                x2 -= salto2;
            }

            if (mouse.Y > (y2+60))
            {
                y2 += salto2;
            }
            else
            {
                y2 -= salto2;
            }

            catalinStalker.Location = new Point(x,y);
            rubenStalker.Location = new Point(x2,y2);

            
        }

        private void catalinStalker_MouseHover(object sender, EventArgs e)
        {
            //MessageBox.Show("Game Over!");
        }

        private void catalinStalker_MouseEnter(object sender, EventArgs e)
        {
            MessageBox.Show("Game Over!");
            this.Close();
            main.Show();
        }
    }
}
