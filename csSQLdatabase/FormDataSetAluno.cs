﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace csSQLdatabase
{
    public partial class FormDataSetAluno : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        Form main;
        public FormDataSetAluno(Form form)
        {
            InitializeComponent();
            main = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            main.Show();
        }

        private void aLunoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.aLunoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        private void FormDataSetAluno_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.ALuno' table. You can move, or remove it, as needed.
            this.aLunoTableAdapter.Fill(this.bDlocalDataSet.ALuno);

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
