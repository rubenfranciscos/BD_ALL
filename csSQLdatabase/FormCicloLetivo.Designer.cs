﻿namespace csSQLdatabase
{
    partial class FormCicloLetivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nCicloLabel;
            System.Windows.Forms.Label descrLabel;
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.bDlocalDataSet = new csSQLdatabase.BDlocalDataSet();
            this.cicloLetivoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cicloLetivoTableAdapter = new csSQLdatabase.BDlocalDataSetTableAdapters.CicloLetivoTableAdapter();
            this.tableAdapterManager = new csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager();
            this.nCicloTextBox = new System.Windows.Forms.TextBox();
            this.descrTextBox = new System.Windows.Forms.TextBox();
            nCicloLabel = new System.Windows.Forms.Label();
            descrLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nCicloLabel
            // 
            nCicloLabel.AutoSize = true;
            nCicloLabel.Location = new System.Drawing.Point(10, 19);
            nCicloLabel.Name = "nCicloLabel";
            nCicloLabel.Size = new System.Drawing.Size(42, 13);
            nCicloLabel.TabIndex = 18;
            nCicloLabel.Text = "n Ciclo:";
            // 
            // descrLabel
            // 
            descrLabel.AutoSize = true;
            descrLabel.Location = new System.Drawing.Point(14, 45);
            descrLabel.Name = "descrLabel";
            descrLabel.Size = new System.Drawing.Size(38, 13);
            descrLabel.TabIndex = 19;
            descrLabel.Text = "Descr:";
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(453, 227);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 17;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.IndianRed;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancelar.Location = new System.Drawing.Point(537, 227);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 16;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // bDlocalDataSet
            // 
            this.bDlocalDataSet.DataSetName = "BDlocalDataSet";
            this.bDlocalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cicloLetivoBindingSource
            // 
            this.cicloLetivoBindingSource.DataMember = "CicloLetivo";
            this.cicloLetivoBindingSource.DataSource = this.bDlocalDataSet;
            // 
            // cicloLetivoTableAdapter
            // 
            this.cicloLetivoTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ALuno_TurmaTableAdapter = null;
            this.tableAdapterManager.ALunoTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CicloLetivoTableAdapter = this.cicloLetivoTableAdapter;
            this.tableAdapterManager.TurmaTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // nCicloTextBox
            // 
            this.nCicloTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cicloLetivoBindingSource, "nCiclo", true));
            this.nCicloTextBox.Enabled = false;
            this.nCicloTextBox.Location = new System.Drawing.Point(58, 16);
            this.nCicloTextBox.Name = "nCicloTextBox";
            this.nCicloTextBox.Size = new System.Drawing.Size(100, 20);
            this.nCicloTextBox.TabIndex = 19;
            // 
            // descrTextBox
            // 
            this.descrTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cicloLetivoBindingSource, "Descr", true));
            this.descrTextBox.Location = new System.Drawing.Point(58, 42);
            this.descrTextBox.Name = "descrTextBox";
            this.descrTextBox.Size = new System.Drawing.Size(100, 20);
            this.descrTextBox.TabIndex = 20;
            // 
            // FormCicloLetivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 262);
            this.Controls.Add(descrLabel);
            this.Controls.Add(this.descrTextBox);
            this.Controls.Add(nCicloLabel);
            this.Controls.Add(this.nCicloTextBox);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancelar);
            this.Name = "FormCicloLetivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormCicloLetivo";
            this.Load += new System.EventHandler(this.FormCicloLetivo_Load);
            this.Shown += new System.EventHandler(this.FormCicloLetivo_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancelar;
        private BDlocalDataSet bDlocalDataSet;
        private System.Windows.Forms.BindingSource cicloLetivoBindingSource;
        private BDlocalDataSetTableAdapters.CicloLetivoTableAdapter cicloLetivoTableAdapter;
        private BDlocalDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox nCicloTextBox;
        private System.Windows.Forms.TextBox descrTextBox;
    }
}