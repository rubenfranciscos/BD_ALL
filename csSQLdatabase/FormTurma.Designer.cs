﻿namespace csSQLdatabase
{
    partial class FormTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nTurmaLabel = new System.Windows.Forms.Label();
            this.cicloLetivonCicloLabel = new System.Windows.Forms.Label();
            this.descriLabel = new System.Windows.Forms.Label();
            this.anoLetivoLabel = new System.Windows.Forms.Label();
            this.nCicloLabel = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.bDlocalDataSet = new csSQLdatabase.BDlocalDataSet();
            this.turmaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.turmaTableAdapter = new csSQLdatabase.BDlocalDataSetTableAdapters.TurmaTableAdapter();
            this.tableAdapterManager = new csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager();
            this.nTurmaTextBox = new System.Windows.Forms.TextBox();
            this.descriTextBox = new System.Windows.Forms.TextBox();
            this.anoLetivoTextBox = new System.Windows.Forms.TextBox();
            this.turmaBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cicloLetivoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cicloLetivoTableAdapter = new csSQLdatabase.BDlocalDataSetTableAdapters.CicloLetivoTableAdapter();
            this.cicloLetivoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cicloLetivoBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.nCicloTextBox = new System.Windows.Forms.TextBox();
            this.comboBoxDropBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // nTurmaLabel
            // 
            this.nTurmaLabel.AutoSize = true;
            this.nTurmaLabel.Location = new System.Drawing.Point(55, 15);
            this.nTurmaLabel.Name = "nTurmaLabel";
            this.nTurmaLabel.Size = new System.Drawing.Size(49, 13);
            this.nTurmaLabel.TabIndex = 16;
            this.nTurmaLabel.Text = "n Turma:";
            // 
            // cicloLetivonCicloLabel
            // 
            this.cicloLetivonCicloLabel.AutoSize = true;
            this.cicloLetivonCicloLabel.Location = new System.Drawing.Point(7, 41);
            this.cicloLetivonCicloLabel.Name = "cicloLetivonCicloLabel";
            this.cicloLetivonCicloLabel.Size = new System.Drawing.Size(97, 13);
            this.cicloLetivonCicloLabel.TabIndex = 17;
            this.cicloLetivonCicloLabel.Text = "Ciclo Letivon Ciclo:";
            // 
            // descriLabel
            // 
            this.descriLabel.AutoSize = true;
            this.descriLabel.Location = new System.Drawing.Point(64, 67);
            this.descriLabel.Name = "descriLabel";
            this.descriLabel.Size = new System.Drawing.Size(40, 13);
            this.descriLabel.TabIndex = 18;
            this.descriLabel.Text = "Descri:";
            // 
            // anoLetivoLabel
            // 
            this.anoLetivoLabel.AutoSize = true;
            this.anoLetivoLabel.Location = new System.Drawing.Point(43, 93);
            this.anoLetivoLabel.Name = "anoLetivoLabel";
            this.anoLetivoLabel.Size = new System.Drawing.Size(61, 13);
            this.anoLetivoLabel.TabIndex = 19;
            this.anoLetivoLabel.Text = "Ano Letivo:";
            // 
            // nCicloLabel
            // 
            this.nCicloLabel.AutoSize = true;
            this.nCicloLabel.Location = new System.Drawing.Point(62, 123);
            this.nCicloLabel.Name = "nCicloLabel";
            this.nCicloLabel.Size = new System.Drawing.Size(42, 13);
            this.nCicloLabel.TabIndex = 20;
            this.nCicloLabel.Text = "n Ciclo:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.IndianRed;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancelar.Location = new System.Drawing.Point(537, 227);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 14;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(456, 227);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // bDlocalDataSet
            // 
            this.bDlocalDataSet.DataSetName = "BDlocalDataSet";
            this.bDlocalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // turmaBindingSource
            // 
            this.turmaBindingSource.DataMember = "Turma";
            this.turmaBindingSource.DataSource = this.bDlocalDataSet;
            // 
            // turmaTableAdapter
            // 
            this.turmaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ALuno_TurmaTableAdapter = null;
            this.tableAdapterManager.ALunoTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CicloLetivoTableAdapter = null;
            this.tableAdapterManager.TurmaTableAdapter = this.turmaTableAdapter;
            this.tableAdapterManager.UpdateOrder = csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // nTurmaTextBox
            // 
            this.nTurmaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "nTurma", true));
            this.nTurmaTextBox.Location = new System.Drawing.Point(110, 12);
            this.nTurmaTextBox.Name = "nTurmaTextBox";
            this.nTurmaTextBox.Size = new System.Drawing.Size(100, 20);
            this.nTurmaTextBox.TabIndex = 17;
            // 
            // descriTextBox
            // 
            this.descriTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "Descri", true));
            this.descriTextBox.Location = new System.Drawing.Point(110, 64);
            this.descriTextBox.Name = "descriTextBox";
            this.descriTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriTextBox.TabIndex = 19;
            // 
            // anoLetivoTextBox
            // 
            this.anoLetivoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "AnoLetivo", true));
            this.anoLetivoTextBox.Location = new System.Drawing.Point(110, 90);
            this.anoLetivoTextBox.Name = "anoLetivoTextBox";
            this.anoLetivoTextBox.Size = new System.Drawing.Size(100, 20);
            this.anoLetivoTextBox.TabIndex = 20;
            // 
            // turmaBindingSource1
            // 
            this.turmaBindingSource1.DataMember = "Turma";
            this.turmaBindingSource1.DataSource = this.bDlocalDataSet;
            // 
            // cicloLetivoBindingSource
            // 
            this.cicloLetivoBindingSource.DataMember = "CicloLetivo";
            this.cicloLetivoBindingSource.DataSource = this.bDlocalDataSet;
            // 
            // cicloLetivoTableAdapter
            // 
            this.cicloLetivoTableAdapter.ClearBeforeFill = true;
            // 
            // cicloLetivoBindingSource1
            // 
            this.cicloLetivoBindingSource1.DataMember = "CicloLetivo";
            this.cicloLetivoBindingSource1.DataSource = this.bDlocalDataSet;
            // 
            // cicloLetivoBindingSource2
            // 
            this.cicloLetivoBindingSource2.DataMember = "CicloLetivo";
            this.cicloLetivoBindingSource2.DataSource = this.bDlocalDataSet;
            // 
            // nCicloTextBox
            // 
            this.nCicloTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cicloLetivoBindingSource2, "nCiclo", true));
            this.nCicloTextBox.Location = new System.Drawing.Point(110, 116);
            this.nCicloTextBox.Name = "nCicloTextBox";
            this.nCicloTextBox.Size = new System.Drawing.Size(100, 20);
            this.nCicloTextBox.TabIndex = 21;
            // 
            // comboBoxDropBox
            // 
            this.comboBoxDropBox.FormattingEnabled = true;
            this.comboBoxDropBox.Location = new System.Drawing.Point(110, 37);
            this.comboBoxDropBox.Name = "comboBoxDropBox";
            this.comboBoxDropBox.Size = new System.Drawing.Size(100, 21);
            this.comboBoxDropBox.TabIndex = 22;
            this.comboBoxDropBox.SelectedIndexChanged += new System.EventHandler(this.comboBoxDropBox_SelectedIndexChanged);
            // 
            // FormTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 270);
            this.Controls.Add(this.comboBoxDropBox);
            this.Controls.Add(this.nCicloLabel);
            this.Controls.Add(this.nCicloTextBox);
            this.Controls.Add(this.anoLetivoLabel);
            this.Controls.Add(this.anoLetivoTextBox);
            this.Controls.Add(this.descriLabel);
            this.Controls.Add(this.descriTextBox);
            this.Controls.Add(this.cicloLetivonCicloLabel);
            this.Controls.Add(this.nTurmaLabel);
            this.Controls.Add(this.nTurmaTextBox);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancelar);
            this.Name = "FormTurma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormTurma";
            this.Load += new System.EventHandler(this.FormTurma_Load);
            this.Shown += new System.EventHandler(this.FormTurma_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cicloLetivoBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnOK;
        private BDlocalDataSet bDlocalDataSet;
        private System.Windows.Forms.BindingSource turmaBindingSource;
        private BDlocalDataSetTableAdapters.TurmaTableAdapter turmaTableAdapter;
        private BDlocalDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox nTurmaTextBox;
        private System.Windows.Forms.TextBox descriTextBox;
        private System.Windows.Forms.TextBox anoLetivoTextBox;
        private System.Windows.Forms.BindingSource turmaBindingSource1;
        private System.Windows.Forms.BindingSource cicloLetivoBindingSource;
        private BDlocalDataSetTableAdapters.CicloLetivoTableAdapter cicloLetivoTableAdapter;
        private System.Windows.Forms.BindingSource cicloLetivoBindingSource1;
        private System.Windows.Forms.BindingSource cicloLetivoBindingSource2;
        private System.Windows.Forms.TextBox nCicloTextBox;
        private System.Windows.Forms.Label nTurmaLabel;
        private System.Windows.Forms.Label cicloLetivonCicloLabel;
        private System.Windows.Forms.Label descriLabel;
        private System.Windows.Forms.Label anoLetivoLabel;
        private System.Windows.Forms.Label nCicloLabel;
        private System.Windows.Forms.ComboBox comboBoxDropBox;
    }
}