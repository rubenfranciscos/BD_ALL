﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;

namespace csSQLdatabase
{
    public partial class FormLista : Form
    {
        Form main;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert
        private SqlConnection sqlConn = null;
        private MySqlConnection mySqlConn = null;
        private SQLiteConnection sqlite = null;
        /*
         * Construtor publico de 4 argumentos, 3 strings e 1 Form para a referencia
         * Origem:
         * -Menu vem um pedido dmlSelect = "Inserir" para um determinado sgbd para um codigo selecionado
         * -FormList vem um sgbd = "Update" ou "Delete" para um sgbd, para um codigo selecionado na DataGrid
         */

        public FormLista(Form form)
        {
            InitializeComponent();
            main = form;
        }

        public FormLista(Form form, String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            main = form;

            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;

            MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }

        /*
         * LOAD: 1º metodo a aser executado, depois do construtor
         * Descrição: Constroi a DataGrid das consultas na FormList
         * Usa os atributos da classe: sgbd e tabela para fazer a query para dataGridView
         *  -sgbd - Comum a todas as consultasde um menu, define o SGBD a usar (Forms multi usos)
         *  -tabela - especifica a tabela a usar: Aluno, Turma ou cicloLetivo
         */
        private void FormLista_Load(object sender, EventArgs e)
        {
            this.Text = BDsgbd + " " + BDdml;

            

            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server":
                            sqlConn = UtilsSQL.getSqlConnSQLServer();
                            //Abre a BD
                            if(UtilsSQL.getTest())
                            {
                                MessageBox.Show("Vou abrir a BD","TESTES: FormLista_Load()");
                            }

                            sqlConn.Open();

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o query","TESTES: FormLista_Load");

                            SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from " + BDcodigo, sqlConn);

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou preenhcer a dataGridView","TESTES: FormLista_Load");
                            DataTable dataTable = new DataTable();
                            dataAdapter.Fill(dataTable);
                            dataGridView1.DataSource = dataTable;
                            break;
                    case "MySQL":

                            mySqlConn = UtilsSQL.getSqlConnMySql();
                            //Abre a BD
                            if (UtilsSQL.getTest())
                            {
                                MessageBox.Show("Vou abrir a BD", "TESTES: FormLista_Load()");
                            }

                            mySqlConn.Open();

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o query", "TESTES: FormLista_Load");

                            MySqlDataAdapter mydataAdapter = new MySqlDataAdapter("Select * from " + BDcodigo, mySqlConn);

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou preenhcer a dataGridView", "TESTES: FormLista_Load");
                            DataTable mydataTable = new DataTable();
                            mydataAdapter.Fill(mydataTable);
                            dataGridView1.DataSource = mydataTable;
                            break;
                    case "SQL Lite":

                            sqlite = UtilsSQL.getSqlConnSQLLite();
                            //Abre a BD
                            if (UtilsSQL.getTest())
                            {
                                MessageBox.Show("Vou abrir a BD", "TESTES: FormLista_Load()");
                            }

                            sqlite.Open();

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o query", "TESTES: FormLista_Load");

                            SQLiteDataAdapter sqLitedataAdapter = new SQLiteDataAdapter("Select * from " + BDcodigo, sqlite);

                            if (UtilsSQL.getTest()) MessageBox.Show("Vou preenhcer a dataGridView", "TESTES: FormLista_Load");
                            DataTable sqLitedataTable = new DataTable();
                            sqLitedataAdapter.Fill(sqLitedataTable);
                            dataGridView1.DataSource = sqLitedataTable;
                            break;
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro a abri a Base dados \n" + ex.Message, "ERRO: Form_Lista_load()");
            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server":
                        sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default: 
                        MessageBox.Show("String errada: " + BDsgbd , "Erro Form aluno - Load");
                        break;
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            main.Show();
        }
        /// <summary>
        /// /////////////////////////////////////////////////////////////
        /// Item listener
        /// Tratamento do evento click sobre os itens clicados na dataGridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                textBox1.Text = row.Cells[0].Value.ToString();
                textBox2.Text = row.Cells[1].Value.ToString();

            }
            catch(ArgumentException ex)
            {
                MessageBox.Show("Erro ArgumentoOutOfRangeException\n" + ex.Message, "FormLista - dataGridView_CellClick");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro a tratar\n" + ex.Message, "FormLista - dataGridView_CellClick");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if( textBox1.Text != "" )
            {
                switch(BDcodigo)
                {
                    case "Aluno":
                    case "aluno":
                        FormAluno formAluno = new FormAluno(main, BDsgbd, "Update", textBox1.Text);
                        this.Close();
                        formAluno.Show();
                        break;
                    case "Turma":
                    case "turma":
                        FormTurma formTurma = new FormTurma(main, BDsgbd, "Update", textBox1.Text);
                        this.Close();
                        formTurma.Show();
                        break;
                    case "CicloLetivo":
                    case "cicloletivo":
                        FormCicloLetivo formCiclo = new FormCicloLetivo(main, BDsgbd, "Update", textBox1.Text);
                        this.Close();
                        formCiclo.Show();
                        break;
                    default:
                        MessageBox.Show("String na tabela Errada: |" + BDcodigo + "|", "ERRO: FormLista - btnAlterar - switch()");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                switch (BDcodigo)
                {
                    case "Aluno":
                    case "aluno":
                        FormAluno formAluno = new FormAluno(main, BDsgbd, "Delete", textBox1.Text);
                        this.Close();
                        formAluno.Show();
                        break;
                    case "Turma":
                    case "turma":
                        FormTurma formTurma = new FormTurma(main, BDsgbd, "Delete", textBox1.Text);
                        this.Close();
                        formTurma.Show();
                        break;
                    case "CicloLetivo":
                    case "cicloletivo":
                        FormCicloLetivo formCiclo = new FormCicloLetivo(main, BDsgbd, "Delete", textBox1.Text);
                        this.Close();
                        formCiclo.Show();
                        break;
                    default:
                        MessageBox.Show("String na tabela Errada: |" + BDcodigo + "|", "ERRO: FormLista - btnEliminar - switch()");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }

        
    }
}
