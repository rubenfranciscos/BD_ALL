﻿namespace csSQLdatabase
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFicheiro = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.messageBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDataSet = new System.Windows.Forms.ToolStripMenuItem();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocal = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDLocalIinserirAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDLocalConsultarAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBDLocalInserirTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDLocalConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBDLocalInserirCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDLocalConsultarCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLserver = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServelIinserirAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServelConsultarAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLServelIinserirTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServelConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLServelIinserirCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServelConsultarCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySQL = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySQLIinserirAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySQLIConsultarAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMySQLIinserirTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySQLConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMySQLIinserirCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySQLConsultarCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLlite = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLliteIinserirAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLliteConsultarAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLliteIinserirTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLliteConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLliteIinserirCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLliteConsultarCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracle = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleIinserirAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleConsultarAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOracleIinserirTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOracleIinserirCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleConsultarCicloLetivo = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonOn = new System.Windows.Forms.RadioButton();
            this.radioButtonOFF = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonBDLocal = new System.Windows.Forms.RadioButton();
            this.radioButtonBDOnline = new System.Windows.Forms.RadioButton();
            this.panel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Location = new System.Drawing.Point(679, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(14, 14);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkBlue;
            this.panel2.Location = new System.Drawing.Point(659, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(14, 14);
            this.panel2.TabIndex = 1;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.Location = new System.Drawing.Point(639, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(14, 10);
            this.panel3.TabIndex = 1;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel4.Location = new System.Drawing.Point(12, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(621, 14);
            this.panel4.TabIndex = 2;
            this.panel4.DragEnter += new System.Windows.Forms.DragEventHandler(this.panel4_DragEnter);
            this.panel4.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.panel4_GiveFeedback);
            this.panel4.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.panel4_QueryContinueDrag);
            this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel4_MouseDown);
            this.panel4.MouseHover += new System.EventHandler(this.panel4_MouseHover);
            this.panel4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel4_MouseMove);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel5.Controls.Add(this.groupBox1);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.menuStrip1);
            this.panel5.Location = new System.Drawing.Point(12, 32);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(681, 374);
            this.panel5.TabIndex = 3;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(636, 334);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(16, 16);
            this.panel6.TabIndex = 1;
            this.panel6.Click += new System.EventHandler(this.panel6_Click);
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSlateGray;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFicheiro,
            this.menuDataSet,
            this.menuBDlocal,
            this.menuSQLserver,
            this.menuMySQL,
            this.menuSQLlite,
            this.menuOracle});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(681, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFicheiro
            // 
            this.menuFicheiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.messageBoxToolStripMenuItem});
            this.menuFicheiro.Name = "menuFicheiro";
            this.menuFicheiro.Size = new System.Drawing.Size(61, 20);
            this.menuFicheiro.Text = "Ficheiro";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // messageBoxToolStripMenuItem
            // 
            this.messageBoxToolStripMenuItem.Name = "messageBoxToolStripMenuItem";
            this.messageBoxToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.messageBoxToolStripMenuItem.Text = "MessageBox";
            // 
            // menuDataSet
            // 
            this.menuDataSet.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.turmaToolStripMenuItem});
            this.menuDataSet.Name = "menuDataSet";
            this.menuDataSet.Size = new System.Drawing.Size(59, 20);
            this.menuDataSet.Text = "DataSet";
            this.menuDataSet.Click += new System.EventHandler(this.dataSetToolStripMenuItem_Click);
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.alunoToolStripMenuItem.Text = "Aluno";
            this.alunoToolStripMenuItem.Click += new System.EventHandler(this.alunoToolStripMenuItem_Click);
            // 
            // turmaToolStripMenuItem
            // 
            this.turmaToolStripMenuItem.Name = "turmaToolStripMenuItem";
            this.turmaToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.turmaToolStripMenuItem.Text = "Turma";
            this.turmaToolStripMenuItem.Click += new System.EventHandler(this.turmaToolStripMenuItem_Click);
            // 
            // menuBDlocal
            // 
            this.menuBDlocal.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBDLocalIinserirAluno,
            this.menuBDLocalConsultarAluno,
            this.toolStripSeparator1,
            this.menuBDLocalInserirTurma,
            this.menuBDLocalConsultarTurma,
            this.toolStripSeparator2,
            this.menuBDLocalInserirCicloLetivo,
            this.menuBDLocalConsultarCicloLetivo});
            this.menuBDlocal.Name = "menuBDlocal";
            this.menuBDlocal.Size = new System.Drawing.Size(59, 20);
            this.menuBDlocal.Text = "BDlocal";
            // 
            // menuBDLocalIinserirAluno
            // 
            this.menuBDLocalIinserirAluno.Name = "menuBDLocalIinserirAluno";
            this.menuBDLocalIinserirAluno.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalIinserirAluno.Text = "Inserir Aluno";
            this.menuBDLocalIinserirAluno.Click += new System.EventHandler(this.menuBDLocalIinserirAluno_Click);
            // 
            // menuBDLocalConsultarAluno
            // 
            this.menuBDLocalConsultarAluno.Name = "menuBDLocalConsultarAluno";
            this.menuBDLocalConsultarAluno.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalConsultarAluno.Text = "Consultar Aluno";
            this.menuBDLocalConsultarAluno.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // menuBDLocalInserirTurma
            // 
            this.menuBDLocalInserirTurma.Name = "menuBDLocalInserirTurma";
            this.menuBDLocalInserirTurma.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalInserirTurma.Text = "Inserir Turma";
            this.menuBDLocalInserirTurma.Click += new System.EventHandler(this.menuBDLocalInserirTurma_Click);
            // 
            // menuBDLocalConsultarTurma
            // 
            this.menuBDLocalConsultarTurma.Name = "menuBDLocalConsultarTurma";
            this.menuBDLocalConsultarTurma.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalConsultarTurma.Text = "Consultar Turma";
            this.menuBDLocalConsultarTurma.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
            // 
            // menuBDLocalInserirCicloLetivo
            // 
            this.menuBDLocalInserirCicloLetivo.Name = "menuBDLocalInserirCicloLetivo";
            this.menuBDLocalInserirCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalInserirCicloLetivo.Text = "Inserir CicloLetivo";
            this.menuBDLocalInserirCicloLetivo.Click += new System.EventHandler(this.menuBDLocalInserirCicloLetivo_Click);
            // 
            // menuBDLocalConsultarCicloLetivo
            // 
            this.menuBDLocalConsultarCicloLetivo.Name = "menuBDLocalConsultarCicloLetivo";
            this.menuBDLocalConsultarCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuBDLocalConsultarCicloLetivo.Text = "Consultar CicloLetivo";
            this.menuBDLocalConsultarCicloLetivo.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // menuSQLserver
            // 
            this.menuSQLserver.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSQLServelIinserirAluno,
            this.menuSQLServelConsultarAluno,
            this.toolStripSeparator3,
            this.menuSQLServelIinserirTurma,
            this.menuSQLServelConsultarTurma,
            this.toolStripSeparator4,
            this.menuSQLServelIinserirCicloLetivo,
            this.menuSQLServelConsultarCicloLetivo});
            this.menuSQLserver.Name = "menuSQLserver";
            this.menuSQLserver.Size = new System.Drawing.Size(75, 20);
            this.menuSQLserver.Text = "SQL Server";
            // 
            // menuSQLServelIinserirAluno
            // 
            this.menuSQLServelIinserirAluno.Name = "menuSQLServelIinserirAluno";
            this.menuSQLServelIinserirAluno.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelIinserirAluno.Text = "Inserir Aluno";
            this.menuSQLServelIinserirAluno.Click += new System.EventHandler(this.menuBDLocalIinserirAluno_Click);
            // 
            // menuSQLServelConsultarAluno
            // 
            this.menuSQLServelConsultarAluno.Name = "menuSQLServelConsultarAluno";
            this.menuSQLServelConsultarAluno.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelConsultarAluno.Text = "Consultar Aluno";
            this.menuSQLServelConsultarAluno.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(184, 6);
            // 
            // menuSQLServelIinserirTurma
            // 
            this.menuSQLServelIinserirTurma.Name = "menuSQLServelIinserirTurma";
            this.menuSQLServelIinserirTurma.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelIinserirTurma.Text = "Inserir Turma";
            this.menuSQLServelIinserirTurma.Click += new System.EventHandler(this.menuBDLocalInserirTurma_Click);
            // 
            // menuSQLServelConsultarTurma
            // 
            this.menuSQLServelConsultarTurma.Name = "menuSQLServelConsultarTurma";
            this.menuSQLServelConsultarTurma.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelConsultarTurma.Text = "Consultar Turma";
            this.menuSQLServelConsultarTurma.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(184, 6);
            // 
            // menuSQLServelIinserirCicloLetivo
            // 
            this.menuSQLServelIinserirCicloLetivo.Name = "menuSQLServelIinserirCicloLetivo";
            this.menuSQLServelIinserirCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelIinserirCicloLetivo.Text = "Inserir CicloLetivo";
            this.menuSQLServelIinserirCicloLetivo.Click += new System.EventHandler(this.menuBDLocalInserirCicloLetivo_Click);
            // 
            // menuSQLServelConsultarCicloLetivo
            // 
            this.menuSQLServelConsultarCicloLetivo.Name = "menuSQLServelConsultarCicloLetivo";
            this.menuSQLServelConsultarCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuSQLServelConsultarCicloLetivo.Text = "Consultar CicloLetivo";
            this.menuSQLServelConsultarCicloLetivo.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // menuMySQL
            // 
            this.menuMySQL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMySQLIinserirAluno,
            this.menuMySQLIConsultarAluno,
            this.toolStripSeparator5,
            this.menuMySQLIinserirTurma,
            this.menuMySQLConsultarTurma,
            this.toolStripSeparator6,
            this.menuMySQLIinserirCicloLetivo,
            this.menuMySQLConsultarCicloLetivo});
            this.menuMySQL.Name = "menuMySQL";
            this.menuMySQL.Size = new System.Drawing.Size(57, 20);
            this.menuMySQL.Text = "MySQL";
            // 
            // menuMySQLIinserirAluno
            // 
            this.menuMySQLIinserirAluno.Name = "menuMySQLIinserirAluno";
            this.menuMySQLIinserirAluno.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLIinserirAluno.Text = "Inserir Aluno";
            this.menuMySQLIinserirAluno.Click += new System.EventHandler(this.menuBDLocalIinserirAluno_Click);
            // 
            // menuMySQLIConsultarAluno
            // 
            this.menuMySQLIConsultarAluno.Name = "menuMySQLIConsultarAluno";
            this.menuMySQLIConsultarAluno.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLIConsultarAluno.Text = "Consultar Aluno";
            this.menuMySQLIConsultarAluno.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(184, 6);
            // 
            // menuMySQLIinserirTurma
            // 
            this.menuMySQLIinserirTurma.Name = "menuMySQLIinserirTurma";
            this.menuMySQLIinserirTurma.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLIinserirTurma.Text = "Inserir Turma";
            this.menuMySQLIinserirTurma.Click += new System.EventHandler(this.menuBDLocalInserirTurma_Click);
            // 
            // menuMySQLConsultarTurma
            // 
            this.menuMySQLConsultarTurma.Name = "menuMySQLConsultarTurma";
            this.menuMySQLConsultarTurma.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLConsultarTurma.Text = "Consultar Turma";
            this.menuMySQLConsultarTurma.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(184, 6);
            // 
            // menuMySQLIinserirCicloLetivo
            // 
            this.menuMySQLIinserirCicloLetivo.Name = "menuMySQLIinserirCicloLetivo";
            this.menuMySQLIinserirCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLIinserirCicloLetivo.Text = "Inserir CicloLetivo";
            this.menuMySQLIinserirCicloLetivo.Click += new System.EventHandler(this.menuBDLocalInserirCicloLetivo_Click);
            // 
            // menuMySQLConsultarCicloLetivo
            // 
            this.menuMySQLConsultarCicloLetivo.Name = "menuMySQLConsultarCicloLetivo";
            this.menuMySQLConsultarCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuMySQLConsultarCicloLetivo.Text = "Consultar CicloLetivo";
            this.menuMySQLConsultarCicloLetivo.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // menuSQLlite
            // 
            this.menuSQLlite.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSQLliteIinserirAluno,
            this.menuSQLliteConsultarAluno,
            this.toolStripSeparator7,
            this.menuSQLliteIinserirTurma,
            this.menuSQLliteConsultarTurma,
            this.toolStripSeparator8,
            this.menuSQLliteIinserirCicloLetivo,
            this.menuSQLliteConsultarCicloLetivo});
            this.menuSQLlite.Name = "menuSQLlite";
            this.menuSQLlite.Size = new System.Drawing.Size(62, 20);
            this.menuSQLlite.Text = "SQL Lite";
            // 
            // menuSQLliteIinserirAluno
            // 
            this.menuSQLliteIinserirAluno.Name = "menuSQLliteIinserirAluno";
            this.menuSQLliteIinserirAluno.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteIinserirAluno.Text = "Inserir Aluno";
            this.menuSQLliteIinserirAluno.Click += new System.EventHandler(this.menuBDLocalIinserirAluno_Click);
            // 
            // menuSQLliteConsultarAluno
            // 
            this.menuSQLliteConsultarAluno.Name = "menuSQLliteConsultarAluno";
            this.menuSQLliteConsultarAluno.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteConsultarAluno.Text = "Consultar Aluno";
            this.menuSQLliteConsultarAluno.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(184, 6);
            // 
            // menuSQLliteIinserirTurma
            // 
            this.menuSQLliteIinserirTurma.Name = "menuSQLliteIinserirTurma";
            this.menuSQLliteIinserirTurma.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteIinserirTurma.Text = "Inserir Turma";
            this.menuSQLliteIinserirTurma.Click += new System.EventHandler(this.menuBDLocalInserirTurma_Click);
            // 
            // menuSQLliteConsultarTurma
            // 
            this.menuSQLliteConsultarTurma.Name = "menuSQLliteConsultarTurma";
            this.menuSQLliteConsultarTurma.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteConsultarTurma.Text = "Consultar Turma";
            this.menuSQLliteConsultarTurma.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(184, 6);
            // 
            // menuSQLliteIinserirCicloLetivo
            // 
            this.menuSQLliteIinserirCicloLetivo.Name = "menuSQLliteIinserirCicloLetivo";
            this.menuSQLliteIinserirCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteIinserirCicloLetivo.Text = "Inserir CicloLetivo";
            this.menuSQLliteIinserirCicloLetivo.Click += new System.EventHandler(this.menuBDLocalInserirCicloLetivo_Click);
            // 
            // menuSQLliteConsultarCicloLetivo
            // 
            this.menuSQLliteConsultarCicloLetivo.Name = "menuSQLliteConsultarCicloLetivo";
            this.menuSQLliteConsultarCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuSQLliteConsultarCicloLetivo.Text = "Consultar CicloLetivo";
            this.menuSQLliteConsultarCicloLetivo.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // menuOracle
            // 
            this.menuOracle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOracleIinserirAluno,
            this.menuOracleConsultarAluno,
            this.toolStripSeparator9,
            this.menuOracleIinserirTurma,
            this.menuOracleConsultarTurma,
            this.toolStripSeparator10,
            this.menuOracleIinserirCicloLetivo,
            this.menuOracleConsultarCicloLetivo});
            this.menuOracle.Name = "menuOracle";
            this.menuOracle.Size = new System.Drawing.Size(53, 20);
            this.menuOracle.Text = "Oracle";
            // 
            // menuOracleIinserirAluno
            // 
            this.menuOracleIinserirAluno.Name = "menuOracleIinserirAluno";
            this.menuOracleIinserirAluno.Size = new System.Drawing.Size(187, 22);
            this.menuOracleIinserirAluno.Text = "Inserir Aluno";
            this.menuOracleIinserirAluno.Click += new System.EventHandler(this.menuBDLocalIinserirAluno_Click);
            // 
            // menuOracleConsultarAluno
            // 
            this.menuOracleConsultarAluno.Name = "menuOracleConsultarAluno";
            this.menuOracleConsultarAluno.Size = new System.Drawing.Size(187, 22);
            this.menuOracleConsultarAluno.Text = "Consultar Aluno";
            this.menuOracleConsultarAluno.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(184, 6);
            // 
            // menuOracleIinserirTurma
            // 
            this.menuOracleIinserirTurma.Name = "menuOracleIinserirTurma";
            this.menuOracleIinserirTurma.Size = new System.Drawing.Size(187, 22);
            this.menuOracleIinserirTurma.Text = "Inserir Turma";
            this.menuOracleIinserirTurma.Click += new System.EventHandler(this.menuBDLocalInserirTurma_Click);
            // 
            // menuOracleConsultarTurma
            // 
            this.menuOracleConsultarTurma.Name = "menuOracleConsultarTurma";
            this.menuOracleConsultarTurma.Size = new System.Drawing.Size(187, 22);
            this.menuOracleConsultarTurma.Text = "Consultar Turma";
            this.menuOracleConsultarTurma.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(184, 6);
            // 
            // menuOracleIinserirCicloLetivo
            // 
            this.menuOracleIinserirCicloLetivo.Name = "menuOracleIinserirCicloLetivo";
            this.menuOracleIinserirCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuOracleIinserirCicloLetivo.Text = "Inserir CicloLetivo";
            this.menuOracleIinserirCicloLetivo.Click += new System.EventHandler(this.menuBDLocalInserirCicloLetivo_Click);
            // 
            // menuOracleConsultarCicloLetivo
            // 
            this.menuOracleConsultarCicloLetivo.Name = "menuOracleConsultarCicloLetivo";
            this.menuOracleConsultarCicloLetivo.Size = new System.Drawing.Size(187, 22);
            this.menuOracleConsultarCicloLetivo.Text = "Consultar CicloLetivo";
            this.menuOracleConsultarCicloLetivo.Click += new System.EventHandler(this.menuBDLocalConsultarAluno_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.radioButtonOFF);
            this.groupBox1.Controls.Add(this.radioButtonOn);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(109, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(60, 64);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Testes";
            // 
            // radioButtonOn
            // 
            this.radioButtonOn.AutoSize = true;
            this.radioButtonOn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.radioButtonOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioButtonOn.Location = new System.Drawing.Point(6, 19);
            this.radioButtonOn.Name = "radioButtonOn";
            this.radioButtonOn.Size = new System.Drawing.Size(41, 17);
            this.radioButtonOn.TabIndex = 0;
            this.radioButtonOn.TabStop = true;
            this.radioButtonOn.Text = "ON";
            this.radioButtonOn.UseVisualStyleBackColor = false;
            this.radioButtonOn.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButtonOFF
            // 
            this.radioButtonOFF.AutoSize = true;
            this.radioButtonOFF.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.radioButtonOFF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioButtonOFF.Location = new System.Drawing.Point(6, 42);
            this.radioButtonOFF.Name = "radioButtonOFF";
            this.radioButtonOFF.Size = new System.Drawing.Size(45, 17);
            this.radioButtonOFF.TabIndex = 1;
            this.radioButtonOFF.TabStop = true;
            this.radioButtonOFF.Text = "OFF";
            this.radioButtonOFF.UseVisualStyleBackColor = false;
            this.radioButtonOFF.CheckedChanged += new System.EventHandler(this.radioButtonOFF_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.radioButtonBDLocal);
            this.groupBox2.Controls.Add(this.radioButtonBDOnline);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(12, 339);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(103, 64);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bases de Dados";
            // 
            // radioButtonBDLocal
            // 
            this.radioButtonBDLocal.AutoSize = true;
            this.radioButtonBDLocal.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.radioButtonBDLocal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioButtonBDLocal.Location = new System.Drawing.Point(6, 42);
            this.radioButtonBDLocal.Name = "radioButtonBDLocal";
            this.radioButtonBDLocal.Size = new System.Drawing.Size(52, 17);
            this.radioButtonBDLocal.TabIndex = 1;
            this.radioButtonBDLocal.TabStop = true;
            this.radioButtonBDLocal.Text = "Ofline";
            this.radioButtonBDLocal.UseVisualStyleBackColor = false;
            this.radioButtonBDLocal.CheckedChanged += new System.EventHandler(this.radioButtonBDLocal_CheckedChanged);
            // 
            // radioButtonBDOnline
            // 
            this.radioButtonBDOnline.AutoSize = true;
            this.radioButtonBDOnline.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.radioButtonBDOnline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioButtonBDOnline.Location = new System.Drawing.Point(6, 19);
            this.radioButtonBDOnline.Name = "radioButtonBDOnline";
            this.radioButtonBDOnline.Size = new System.Drawing.Size(55, 17);
            this.radioButtonBDOnline.TabIndex = 0;
            this.radioButtonBDOnline.TabStop = true;
            this.radioButtonBDOnline.Text = "Online";
            this.radioButtonBDOnline.UseVisualStyleBackColor = false;
            this.radioButtonBDOnline.CheckedChanged += new System.EventHandler(this.radioButtonBDOnline_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(705, 418);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFicheiro;
        private System.Windows.Forms.ToolStripMenuItem menuDataSet;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocal;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalIinserirAluno;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalConsultarAluno;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalInserirTurma;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalInserirCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuBDLocalConsultarCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuSQLserver;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelIinserirAluno;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelConsultarAluno;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelIinserirTurma;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelIinserirCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServelConsultarCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuMySQL;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLIinserirAluno;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLIConsultarAluno;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLIinserirTurma;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLIinserirCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuMySQLConsultarCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuSQLlite;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteIinserirAluno;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteConsultarAluno;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteIinserirTurma;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteIinserirCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuSQLliteConsultarCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuOracle;
        private System.Windows.Forms.ToolStripMenuItem menuOracleIinserirAluno;
        private System.Windows.Forms.ToolStripMenuItem menuOracleConsultarAluno;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem menuOracleIinserirTurma;
        private System.Windows.Forms.ToolStripMenuItem menuOracleConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem menuOracleIinserirCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem menuOracleConsultarCicloLetivo;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem messageBoxToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonOFF;
        private System.Windows.Forms.RadioButton radioButtonOn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonBDLocal;
        private System.Windows.Forms.RadioButton radioButtonBDOnline;
    }
}

