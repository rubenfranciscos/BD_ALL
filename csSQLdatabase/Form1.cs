﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace csSQLdatabase
{

    public partial class Form1 : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        Boolean max = false;

        String sgbd, dml, codigo;

        int x, y, newx, newy, winx, winy;
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            if (max == false)
            {
                ActiveForm.WindowState = FormWindowState.Maximized;
                max = true;
            }     
            else
            {
                ActiveForm.WindowState = FormWindowState.Normal;
                max = false;
            }
                
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            ActiveForm.WindowState = FormWindowState.Minimized;
        }

        private void panel4_DragEnter(object sender, DragEventArgs e)
        {
            //Console.WriteLine("enter");
            //MessageBox.Show("enter");
        }

        private void panel4_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            //Console.WriteLine("query");
            //MessageBox.Show("query");
        }

        private void panel4_MouseDown(object sender, MouseEventArgs e)
        {
            /*Console.WriteLine("enter");
            x = e.X;
            y = e.Y;
            winx = Location.X;
            winy = Location.Y;
            Console.WriteLine("enter " + x + " " + y);*/
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panel4_MouseHover(object sender, EventArgs e)
        {
            Console.WriteLine("hover");
        }

        private void panel4_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            Console.WriteLine("feedback");
        }

        private void panel4_MouseMove(object sender, MouseEventArgs e)
        {
            /*
            if (e.Button == MouseButtons.Left)
            {

                Console.WriteLine("-----------------");
                newx = e.X- x;
                newy = e.Y - y;

                Console.WriteLine("move " + newx + " " + newy);
                this.Location = new Point(winx - newx, winy - newy);

            }*/

        }

        private void dataSetToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void alunoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FormDataSetAluno formAluno = new FormDataSetAluno(this);
            this.Hide();
            formAluno.Show();
        }

        private void turmaToolStripMenuItem_Click(object sender, EventArgs e)
        {


            FormDataSetTurma formAluno = new FormDataSetTurma(this);
            this.Hide();
            formAluno.Show();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void panel6_Click(object sender, EventArgs e)
        {
            /*for (int i = 0; i < 10; i++)
            {
                MessageBox.Show("Oi");
            }*/
            Jayce j = new Jayce(this);
            this.Hide();
            j.Show();
        }

        private void menuBDLocalIinserirAluno_Click(object sender, EventArgs e)
        {
            //((ToolStripMenuItem)sender)
            sgbd = (sender as ToolStripMenuItem).OwnerItem.Text;
            dml = (sender as ToolStripMenuItem).Text;
            codigo = dml.Substring(dml.IndexOf(" ")+1);
            dml = dml.Substring(0, dml.IndexOf(" "));

            FormAluno formA = new FormAluno(this, sgbd, dml, codigo);
            this.Hide();
            formA.Show();

            //UtilsSQL.selectSgbd(sgbd);
        }

        private void menuBDLocalInserirTurma_Click(object sender, EventArgs e)
        {
            sgbd = (sender as ToolStripMenuItem).OwnerItem.Text;
            dml = (sender as ToolStripMenuItem).Text;
            codigo = dml.Substring(dml.IndexOf(" ") + 1);
            dml = dml.Substring(0, dml.IndexOf(" "));

            FormTurma formT = new FormTurma(this, sgbd, dml, codigo);
            this.Hide();
            formT.Show();
        }

        private void menuBDLocalInserirCicloLetivo_Click(object sender, EventArgs e)
        {
            sgbd = (sender as ToolStripMenuItem).OwnerItem.Text;
            dml = (sender as ToolStripMenuItem).Text;
            codigo = dml.Substring(dml.IndexOf(" ") + 1);
            dml = dml.Substring(0, dml.IndexOf(" "));

            FormCicloLetivo formC = new FormCicloLetivo(this, sgbd, dml, codigo);
            this.Hide();
            formC.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void menuBDLocalConsultarAluno_Click(object sender, EventArgs e)
        {
            sgbd = (sender as ToolStripMenuItem).OwnerItem.Text;
            dml = (sender as ToolStripMenuItem).Text;
            codigo = dml.Substring(dml.IndexOf(" ") + 1);
            dml = dml.Substring(0, dml.IndexOf(" "));

            FormLista formLista = new FormLista(this, sgbd, dml, codigo);
            this.Hide();
            formLista.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (UtilsSQL.getTest())
            {
                radioButtonOn.Checked = true;
            }
            else
            {
                radioButtonOFF.Checked = true;
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            UtilsSQL.setTest(true);
        }

        private void radioButtonOFF_CheckedChanged(object sender, EventArgs e)
        {
            UtilsSQL.setTest(false);
        }

        private void radioButtonBDOnline_CheckedChanged(object sender, EventArgs e)
        {
            UtilsSQL.setBDOnline(true);
            menuDataSet.Enabled = false;
            menuBDlocal.Enabled = false;
            menuSQLserver.Enabled = false;
            menuSQLlite.Enabled = false;
            menuOracle.Enabled = false;

        }

        private void radioButtonBDLocal_CheckedChanged(object sender, EventArgs e)
        {
            UtilsSQL.setBDOnline(true);
            menuDataSet.Enabled = true;
            menuBDlocal.Enabled = true;
            menuSQLserver.Enabled = true;
            menuSQLlite.Enabled = true;
            menuOracle.Enabled = true;
        }
    }
}
