﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;

namespace csSQLdatabase
{
    /*
         * Classe static, para não gerar objetos e reter dados comuns
         * Todos os seus membros devem ser static, caso contrario nao serão visiveis
         * Adicionar a livraria System.Windows.Forms para interagir com as Forms
         * Adicionar livraria System.Data.SqlClient, para objetos sql
         */
    static class UtilsSQL//Tem de ser static para nao criar objetos
    {
        //Atributos
        //Atributos sempre static e privados

        static bool test = true; //ativa e desativa as mensagens de testes

        /*
         * Atributos de ligação à BD: 2 por Base de Dados a contactar
         * -connectionString para o endereço e parametros da base de dados
         * -SQlConnection praa converter a string num formato interpretado pelo SGBD
         * 
         * Nota1: Para obter as connString: Propriedades da Base de Dados (View Server Explorer)
         * Nota2: Se localDB e se usar GIT, o endereço tem quer ser adaptado
         * 1 Colar connString no editor e observar o 2º argumento: AttacchDBfileName=AttachDbFilename
         * ="D:\Projeto\C#\Aulas C#\csSQL\BDlocal.mdf"
         * 2 Apagar tudo o que está entre aspas(endereço e nome da db):
         * 3 Substituir por: |DataDirectory|\nome da bd
         * 
         * Nota3: Inserir o caracter @ antes da string - invalida queixas do compilador quando
         * encontrar o carater \ na string.
         */

        // LocalDB - é uma instancia SQL server express do Visual Studio (ler nota 2)
        private static String sqlConnStringBDlocal = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\BDlocal.mdf;Integrated Security=True";
        //Connection - Converte a connectionString para um formato que o SGBD entende
        private static SqlConnection sqlConnBDlocal = new SqlConnection(sqlConnStringBDlocal);

        // SQL Server ou Server Express - Copiar ConnectionString das propriedades da base de dados
        private static String sqlConnStringSQLserver = @"Data Source=LAPTOP-PHQ53H27\SQLEXPRESS;Initial Catalog=Escola;Integrated Security=True";
        //Connection - Converte a connectionString para um formato que o SGBD entende
        private static SqlConnection sqlConnSQLserver = new SqlConnection(sqlConnStringSQLserver);

        /*MySQL*/
        private static String sqlConnStringMySQL = @"server=localhost;user id=root;database=escola";

        private static String sqlConnStringMySQLOnline = @"SERVER=sql4.freemysqlhosting.net;port=3306;DATABASE=sql498437;username=sql498437;password=AkKfS3nW8R";


        //Connection - Converte a connectionString para um formato que o SGBD entende
        private static MySqlConnection sqlConnMySQL = null;

        private static bool bdOnline = false;

        





        /*SQLlite*/
        private static String sqlConnStringSQLlite = "data source=" + System.IO.Path.GetFullPath(@"..\..\") + "escola";
        //Connection - Converte a connectionString para um formato que o SGBD entende
        private static SQLiteConnection sqlConnSQLlite = new SQLiteConnection(sqlConnStringSQLlite);

        /*Oracle*/
        private static String sqlConnStringOracle = "";
        //Connection - Converte a connectionString para um formato que o SGBD entende
        private static SqlConnection sqlConnOracle = new SqlConnection(sqlConnStringOracle);

        
        
        
        public static Object selectSgbd(String sgbd)
        {
            Object sqlConn = null;

            try
            {
                switch(sgbd)    //Seja qual for o SGBD a usar, este switch conce
                {
                        //BDlocal
                    case "BDlocal":
                    case "bdlocal":
                    case "local":
                    case "Local":
                        sqlConn = sqlConnBDlocal;
                        break;
                        //SQL Server
                    case "sqlserver":
                    case "SQLserver":
                    case "SQL Server":
                    case "SQL server":
                    case "SqlServer":
                        sqlConn = sqlConnSQLserver;
                        break;
                        //My SQL
                    case "MySQL":
                    case "mysql":
                    case "MySql":
                        sqlConn = sqlConnMySQL;
                        break;
                        //SQL lite
                    case "sqlite":
                    case "SQLite":
                    case "Sqlite":
                    case "SQL Lite":
                        sqlConn = sqlConnSQLlite;
                        break;
                        //Oracle
                    case "Oracle":
                    case "oracle":
                        sqlConn = sqlConnOracle;
                        break;
                    default:
                        MessageBox.Show("String errada: " + sgbd, "Err: Utils - selectSgbd - switch(sgbd)");
                        break;
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro");
            }
            return sqlConn;
        }

        static public Boolean getTest()
        {
            return test;
        }

        static public void setTest(Boolean t)
        {
            test = t;
        }
        static public SqlConnection getSqlConnBDLocal()
        {
            return sqlConnBDlocal;
        }

        static public SqlConnection getSqlConnSQLServer()
        {
            return sqlConnSQLserver;
        }

        static public MySqlConnection getSqlConnMySql()
        {
            if(bdOnline)
            {
                sqlConnMySQL = new MySqlConnection(sqlConnStringMySQLOnline);
            }
            else
            {
                sqlConnMySQL = new MySqlConnection(sqlConnStringMySQL);
            }
            return sqlConnMySQL;
        }

        static public SQLiteConnection getSqlConnSQLLite()
        {
            return sqlConnSQLlite;
        }
        static public SqlConnection getSqlConnOracle()
        {
            return sqlConnOracle;
        }

        internal static void setBDOnline(bool p)
        {
            bdOnline = p;
        }
        internal static bool getBDOnline()
        {
            return bdOnline;
        }
    }
}
