﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SQLite;

namespace csSQLdatabase
{
    public partial class FormTurma : Form
    {
        Form main;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert
        private SqlTransaction sqlTran = null;
        private MySqlTransaction mySqlTran = null;
        private SQLiteTransaction sqliteTran = null;
        /*
         * Construtor publico de 4 argumentos, 3 strings e 1 Form para a referencia
         * Origem:
         * -Menu vem um pedido dmlSelect = "Inserir" para um determinado sgbd para um codigo selecionado
         * -FormList vem um sgbd = "Update" ou "Delete" para um sgbd, para um codigo selecionado na DataGrid
         */
        public FormTurma(Form form)
        {
            InitializeComponent();
            main = form;
        }

        public FormTurma(Form form, String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            main = form;

            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;

            
        }

        private void FormTurma_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.CicloLetivo' table. You can move, or remove it, as needed.
            this.cicloLetivoTableAdapter.Fill(this.bDlocalDataSet.CicloLetivo);

            // TODO: This line of code loads data into the 'bDlocalDataSet.Turma' table. You can move, or remove it, as needed.
            this.turmaTableAdapter.Fill(this.bDlocalDataSet.Turma);

            //Adiciona o nome da BD e do comnado sql a usar no botão OK, ao titulo da form
            this.Text = BDsgbd + " " + BDdml;


            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();     //Recebe a UtilsSQl uma ligação ao sgbd
            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES turma");
                
                        SqlCommand sqlCommand = new SqlCommand("Select Descr from CicloLetivo", sqlConn);    //Comando SQL DML
                        //sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo); 
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        Console.WriteLine(sqlDataReader.FieldCount);
                        if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                             //Enquanto houver rows
                            {
                                Console.WriteLine(sqlDataReader["Descr"].ToString());
                                comboBoxDropBox.Items.Add(sqlDataReader["Descr"].ToString());
                            }
                        }
                        break;

                    case "MySQL":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES turma");
                
                        MySqlCommand mySqlCommand = new MySqlCommand("Select Descr from CicloLetivo", mySqlConn);    //Comando SQL DML
                        //sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo); 
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        Console.WriteLine(mySqlDataReader.FieldCount);
                        if (mySqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                             //Enquanto houver rows
                            {
                                Console.WriteLine(mySqlDataReader["Descr"].ToString());
                                comboBoxDropBox.Items.Add(mySqlDataReader["Descr"].ToString());
                            }
                        }
                        break;

                    case "SQL Lite":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES turma");
                
                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select Descr from CicloLetivo", sqlite);    //Comando SQL DML
                        //sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo); 
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        Console.WriteLine(sqLiteDataReader.FieldCount);
                        if (sqLiteDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqLiteDataReader.Read())                             //Enquanto houver rows
                            {
                                Console.WriteLine(sqLiteDataReader["Descr"].ToString());
                                comboBoxDropBox.Items.Add(sqLiteDataReader["Descr"].ToString());
                            }
                        }
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - BtnOK - finaly");
                        break;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source.ToString() + "\n" + ex.TargetSite.ToString() + "\n" + ex.Message, "ERRO: FormAluno");

            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form aluno - BtnOK - finaly");
                        break;
                }
            }

            //MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);

            //Alterar o nome do botão ok, de acordo com o dml
            btnOK.Text = BDdml;

            //se Insert, query à BD para obter o ultimo codigo pk

            //Se Update, usar o código passado no construtor para recolher os dados da BD

            //Se delete, usar o codigo passado no construtor para recolher os dados da BD
            //Definir os campos disable

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void turmaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.turmaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            main.Show();
        }

        private void FormTurma_Shown(object sender, EventArgs e)
        {
            MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }


        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();     //Recebe a UtilsSQl uma ligação ao sgbd
            try
            {

                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        SqlCommand sqlCommand = new SqlCommand("Select MAX(nTurma) as nTurma from Turma", sqlConn);   //Comando SQL DML
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqlDataReader["nTurma"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; Turma");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; Turma");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        }
                        break;

                    case "MySQL":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        MySqlCommand mySqlCommand = new MySqlCommand("Select MAX(nTurma) as nTurma from Turma", mySqlConn);   //Comando SQL DML
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mySqlDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = mySqlDataReader["nTurma"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; Turma");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; Turma");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        };
                        break;

                    case "SQL Lite":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select MAX(nTurma) as nTurma from Turma", sqlite);   //Comando SQL DML
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqLiteDataReader.HasRows)                                   //Se houver rows lidas BD
                        {
                            while (sqLiteDataReader.Read())                             //Enquanto houver rows
                            {
                                String temp = sqLiteDataReader["nTurma"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                                if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; Turma");
                                if (temp == "")
                                {
                                    MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; Turma");
                                    pkCode = int.Parse(temp) + 1;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                            pkCode = 1;
                        };
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                        break;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: Turma");

            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                        break;
                }
            }
            return pkCode.ToString();

        }

        private void getFieldsData()
        {
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();

            try
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        SqlCommand sqlCommand = new SqlCommand("Select nTurma, Descri from Turma where nTurma = @codigo", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                            //Enquanto houver rows
                            {
                                //textBoxNome.Text = sqlDataReader["Nome"].ToString();
                                //textBoxNumAluno.Text = sqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                        break;

                    case "MySQL":
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        MySqlCommand mySqlCommand = new MySqlCommand("Select nTurma, Descri from Turma where nTurma = @codigo", mySqlConn);    //Comando SQL DML
                        mySqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (mySqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (mySqlDataReader.Read())                            //Enquanto houver rows
                            {
                                //textBoxNome.Text = sqlDataReader["Nome"].ToString();
                                //textBoxNumAluno.Text = sqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                        break;

                    case "SQL Lite": 
                        if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select nTurma, Descri from Turma where nTurma = @codigo", sqlite);    //Comando SQL DML
                        sqLiteCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqlite.Open();                                             //abertura a base de dados
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqLiteDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqLiteDataReader.Read())                            //Enquanto houver rows
                            {
                                //textBoxNome.Text = sqlDataReader["Nome"].ToString();
                                //textBoxNumAluno.Text = sqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                        break;
                }
                

            }
            catch (Exception e)
            {
                MessageBox.Show("Erro BD:\n" + e.Message, "Turma - getFieldsData()");
            }
            finally
            {
                switch (BDsgbd)
                {
                    case "BDlocal":
                    case "SQL Server": sqlConn.Close();
                        break;

                    case "MySQL":
                        mySqlConn.Close();
                        break;

                    case "SQL Lite":
                        sqlite.Close();
                        break;

                    default:
                        MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                        break;
                }
            }
        }

        private void descriLabel1_Click(object sender, EventArgs e)
        {

        }

        private void descriTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void turmaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nCicloLabel_Click(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = UtilsSQL.getSqlConnSQLServer();
            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql();
            SQLiteConnection sqlite = UtilsSQL.getSqlConnSQLLite();
            /*
             * SQL Transaction
             * Dá ordem ao SGBD para:
             * 1 Bloquear os objetos da base dados envolvidos
             * 2 Executa os comandos DML
             * 3 Liberta os objetos
             * 
             * Se erro num dos comandos DML
             * -> desfaz todos os outros (tenta)
             */

            //validação de dados
            if (nCicloTextBox.Text == "" || descriTextBox.Text == "")
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server": 
                            switch (BDdml)
                            {
                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlInsert = new SqlCommand("Insert into Turma (nTurma, CicloLetivonCiclo, Descri, AnoLetivo) VALUES(@nTurma, @CicloLetivonCiclo, @Descri, @AnoLetivo)", sqlConn);
                                    sqlInsert.Parameters.AddWithValue("@nTurma", int.Parse(getLastTablePk()));
                                    sqlInsert.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqlInsert.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqlInsert.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL

                                    sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                                    sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlUpdate = new SqlCommand("Update Turma SET Descri = @Descr WHERE nTurma = @nTurma", sqlConn);
                                    sqlUpdate.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    sqlUpdate.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqlUpdate.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqlUpdate.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SqlCommand sqlDelete = new SqlCommand("Delete from Turma where nTurma = @nTurma", sqlConn);
                                    sqlDelete.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    sqlDelete.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqlDelete.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqlDelete.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlConn.Open();                                                 //Abertura a base de dados
                                    sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: Turma - Botão OK - switch default");
                                    break;

                            }
                            break;

                        case "MySQL": 
                            switch (BDdml)
                            {
                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlInsert = new MySqlCommand("Insert into Turma (nTurma, CicloLetivonCiclo, Descri, AnoLetivo) VALUES(@nTurma, @CicloLetivonCiclo, @Descri, @AnoLetivo)", mySqlConn);
                                    mySqlInsert.Parameters.AddWithValue("@nTurma", int.Parse(getLastTablePk()));
                                    mySqlInsert.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    mySqlInsert.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    mySqlInsert.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    mySqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                    mySqlTran = mySqlConn.BeginTransaction();               //transação para controlo da operação SQL

                                    mySqlInsert.Transaction = mySqlTran;                    //Ligação dos comandos à transação
                                    mySqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    mySqlTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlUpdate = new MySqlCommand("Update Turma SET Descr = @Descr WHERE nTurma = @nTurma", mySqlConn);
                                    mySqlUpdate.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    mySqlUpdate.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    mySqlUpdate.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    mySqlUpdate.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    MySqlCommand mySqlDelete = new MySqlCommand("Delete from Turma where nTurma = @nTurma", mySqlConn);
                                    mySqlDelete.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    mySqlDelete.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    mySqlDelete.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    mySqlDelete.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    mySqlConn.Open();                                                 //Abertura a base de dados
                                    mySqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: Turma - Botão OK - switch default");
                                    break;

                            }
                            break;

                        case "SQL Lite": 
                            switch (BDdml)
                            {
                                case "Inserir":

                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqLiteInsert = new SQLiteCommand("Insert into Turma (nTurma, CicloLetivonCiclo, Descri, AnoLetivo) VALUES(@nTurma, @CicloLetivonCiclo, @Descri, @AnoLetivo)", sqlite);
                                    sqLiteInsert.Parameters.AddWithValue("@nTurma", int.Parse(getLastTablePk()));
                                    sqLiteInsert.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqLiteInsert.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqLiteInsert.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlite.Open();                                     //abertura da ligaçã ao sgbd
                                    sqliteTran = sqlite.BeginTransaction();               //transação para controlo da operação SQL

                                    sqLiteInsert.Transaction = sqliteTran;                    //Ligação dos comandos à transação
                                    sqLiteInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                    sqliteTran.Commit();                                   //Commit the transaction

                                    break;
                                case "Update":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqLiteUpdate = new SQLiteCommand("Update Turma SET Descr = @Descr WHERE nTurma = @nTurma", sqlite);
                                    sqLiteUpdate.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    sqLiteUpdate.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqLiteUpdate.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqLiteUpdate.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqLiteUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTest()) MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");

                                    SQLiteCommand sqLiteDelete = new SQLiteCommand("Delete from Turma where nTurma = @nTurma", sqlite);
                                    sqLiteDelete.Parameters.AddWithValue("@nTurma", int.Parse(BDcodigo));
                                    sqLiteDelete.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(comboBoxDropBox.Text.Substring(0, 1)));
                                    sqLiteDelete.Parameters.AddWithValue("@Descri", descriTextBox.Text);
                                    sqLiteDelete.Parameters.AddWithValue("@AnoLetivo", int.Parse(anoLetivoTextBox.Text));

                                    sqlite.Open();                                                 //Abertura a base de dados
                                    sqLiteDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                    break;

                                default:
                                    MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: Turma - Botão OK - switch default");
                                    break;

                            }
                            break;

                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                            break;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: Turma - Botão OK - switch()");
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: Turma - Botão OK - switch rollback");
                    }
                }
                finally
                {
                    switch (BDsgbd)
                    {
                        case "BDlocal":
                        case "SQL Server": sqlConn.Close();
                            break;

                        case "MySQL":
                            mySqlConn.Close();
                            break;

                        case "SQL Lite":
                            sqlite.Close();
                            break;

                        default:
                            MessageBox.Show("String errada: " + BDsgbd, "Erro Form Turma - BtnOK - finaly");
                            break;
                    }
                    MessageBox.Show("Procedimento concuido", "INFO");
                    main.Show();
                    this.Close();
                }

            }
        }
    }
}
